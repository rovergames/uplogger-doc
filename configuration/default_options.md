# Default options

```js
{
    stdout: process.stdout, // stream to write log
    stderr: process.stderr, // stream to write error
    useGlobal: true, // override global console or use UpLog instance
    separator: '->', // Separator before the log (See [logLevel options](/configuration/log_level_config.md))
    logLevel: {}, // override default LogLevel config or add custom ones
}
```
