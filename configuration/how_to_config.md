# How to config

You can pass an object of options when you init _UpLogger_:

```js
const UpLogger = require('uplogger');

const options = {
    useGlobal: false,
}

new UpLogger(options);
```

\(See [Default options](/configuration/default_options.md))
