# Configuration

* [How to config](/configuration/how_to_config.md)
* [Default options](configuration/default_options.md)
* [Default log level config](configuration/log_level_config.md)
* [Template configuration](configuration/template.md)
