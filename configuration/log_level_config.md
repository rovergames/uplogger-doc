# Default LogLevel configuration

```js
{
    SUCCESS: {
        id: 0, // id of the logLevel
        label: 'SUCCESS', // label
        lblColor: chalk.bgGreen, // label's color (using chalk)
        msgColor: chalk.green, // messages's color (using chalk)
        separatorColor: chalk.white, // separator color (using chalk)
        template: '#D: #N #S #M' // template of the log for this level (see templates configuration)
    },
    INFO: {
        id: 1,
        label: 'INFO',
        lblColor: chalk.white,
        msgColor: chalk.white,
        separatorColor: chalk.white,
        template: '#D: #N #S #M'
    },
    DEBUG: {
        id: 2,
        label: 'DEBUG',
        lblColor: chalk.bgCyan,
        msgColor: chalk.cyan,
        separatorColor: chalk.white,
        template: '#D: #N: #F:#L:#C #S #M'
    },
    WARN: {
        id: 3,
        label: 'WARN',
        lblColor: chalk.bgYellow,
        msgColor: chalk.yellow,
        separatorColor: chalk.white,
        template: '#D: #N: #F:#L:#C #S #M'
    },
    ERROR: {
        id: 4,
        label: 'ERROR',
        lblColor: chalk.bgRed,
        msgColor: chalk.red,
        separatorColor: chalk.white,
        template: '#D: #N: #F:#L:#C #S #M'
    },
}
```
