# console.debug(msg, [..args])

_UpLogger_ comes with a custom method to log.

`console.debug()` uses the DEBUG log level.

`console.debug()` writes to stdout stream.

```js
const UpLogger = require('uplogger');

new UpLogger();

console.debug('This is a debug');
//prints (with default config):
// {date}: DEBUG: file:line:column -> This is an debug

const arg = 'arg';
console.debug('This is a debug with %s', arg);
//prints (with default config):
// {date}: DEBUG: file:line:column -> This is an debug with arg
```
