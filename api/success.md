# console.success(msg, [..args])

_UpLogger_ comes with a custom method to log.

`console.success()` uses the SUCCESS log level.

`console.success()` writes to stdout stream.

```js
const UpLogger = require('uplogger');

new UpLogger();

console.success('This is a success');
//prints (with default config):
// {date}: SUCCESS -> This is an success

const arg = 'arg';
console.success('This is a success with %s', arg);
//prints (with default config):
// {date}: SUCCESS -> This is an success with arg
```
