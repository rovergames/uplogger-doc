# console.log(msg, [..args])

Use this method like the default one.

`console.log()` uses the INFO log level.

`console.log()` writes to stdout stream.

`console.info()` is an alias for `console.log()`.
```js
const UpLogger = require('uplogger');

new UpLogger();

console.log('This is an info');
//prints (with default config):
// {date}: INFO -> This is an info

const arg = 'arg';
console.log('This is an info with %s', arg);
//prints (with default config):
// {date}: INFO -> This is an info with arg
```
