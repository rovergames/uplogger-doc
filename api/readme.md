# API

* [console.log()](/api/log.md)
* [console.success()](/api/success.md)
* [console.debug()](/api/debug.md)
* [console.warn()](/api/warn.md)
* [console.error()](/api/error.md)
