# console.write(logLevel id, msg, [..args])

_UpLogger_ comes with a custom method to log.

Use `console.write()` to log with custom log level.

`console.write()` writes to stdout stream.

```js
const UpLogger = require('uplogger');

const customLogLvl = {
    CUSTOM: {
        id: 5, // id of the logLevel
        label: 'CUSTOM', // label
        lblColor: chalk.bgRed, // label's color (using chalk)
        msgColor: chalk.red, // messages's color (using chalk)
        separatorColor: chalk.white, // separator color (using chalk)
        template: '#D: #N #S #M' // template of the log for this level
    },
}

new UpLogger({ logLevel: customLogLvl });

console.write(customLogLvl.CUSTOM.id, 'This is a custom log');
//prints (with default config):
// {date}: CUSTOM -> This is a custom log

const arg = 'arg';
console.write(customLogLvl.CUSTOM.id, 'This is a custom log with %s', arg);
//prints (with default config):
// {date}: CUSTOM -> This is a custom log with arg
```

(see [templates configuration](/configuration/template.md))
