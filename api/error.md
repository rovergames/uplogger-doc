# console.error(msg, [..args])

Use this method like the default one.

`console.error()` uses the ERROR log level.

`console.error()` writes to stderr stream.

```js
const UpLogger = require('uplogger');

new UpLogger();

console.error('This is an error');
//prints (with default config):
// {date}: ERROR: file:line:column -> This is an error

const arg = 'arg';
console.error('This is an error with %s', arg);
//prints (with default config):
// {date}: ERROR: file:line:column -> This is an error with arg
```
