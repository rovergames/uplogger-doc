# console.warn(msg, [..args])

Use this method like the default one.

`console.warn()` uses the WARN log level.

`console.warn()` writes to stderr stream.

```js
const UpLogger = require('uplogger');

new UpLogger();

console.error('This is an warn');
//prints (with default config):
// {date}: WARN: file:line:column -> This is an warn

const arg = 'arg';
console.error('This is an warn with %s', arg);
//prints (with default config):
// {date}: WARN: file:line:column -> This is an warn with arg
```
