# UpLogger

_UpLogger_ allows you to override the global console to make the log better !

_Documentation for UpLogger npm module._

_[GitLab project](https://gitlab.com/rovergames/uplogger)_ - _[NPM homepage](https://www.npmjs.com/package/uplogger)_

Made with <3 by [rovergames](/https://twitter.com/rovergamesIsDev)
