# Initialize

Initialize** **_UpLogger_ is very simple ! You just have to import the module and instanciate a new instance.

```js
const UpLogger = require('uplogger');

new UpLogger();
```

And it's done. Now, you can log with _UpLogger_ like you have always log !
