## Installation

Install _UpLogger_ like a classic NPM module:

```bash
$ npm i uplogger
```
